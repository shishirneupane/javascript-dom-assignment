// list the names

let studentsList = document.getElementById("list-students");

const studentsArray = [
  "Shishir Neupane", "Ayush Tamang", "Robin Shrestha", "Kabita Bhandari", "Sameer Gurung",
  "Shikhar Joshi", "Prajwal Paudel", "Sandip Thapa", "Krishna Joshi", "Ganesh Manandhar"
];

for (let i = 0; i < studentsArray.length; i++) {
  studentsList.innerHTML += "<li>" + studentsArray[i] + "</li>";
}


// registration form

let inputs = document.getElementsByTagName("input");
let countrySelect = document.getElementById("country");
const submitBtn = document.getElementById("btn-submit");
let outputText = document.getElementById("output-text");

let firstName = inputs[0];
let lastName = inputs[1];
let email = inputs[2];
let phone = inputs[3];

submitBtn.addEventListener("click", submitHandler);

function submitHandler() {
  if (firstName.value === "" || lastName.value === "" || email.value === "" || phone.value === "" || countrySelect.value === "") {
    console.log("empty");
    outputText.innerText = "Enter all the above fields. None should be empty.";
  }
  else {
    outputText.innerHTML = `
      Hello, <strong>${firstName.value} ${lastName.value}.</strong> <br>
      Your email is <strong>${email.value}</strong> and phone number is <strong>${phone.value}.</strong> <br>
      And, you are from <strong>${countrySelect.value}.</strong> <hr>
      Thank you for filling up the registration form.
    `;
  }
}